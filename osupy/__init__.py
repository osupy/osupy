"""
Project: osupy

License MIT: https://mit-license.org/
Copyright © 2021 - LostPy
"""
__version__ = "0.1.5"

__PANDA_NOT_FOUND_MESSAGE = "This function requires the pandas package which was not found."\
                            " Please, install pandas to use this method: `pip install pandas`"

__PYDUB_NOT_FOUND_MESSAGE = "This function requires the pydub package which was not found."\
                            " Please, install pydub to use this method: `pip install pydub`."\
                            "See https://github.com/jiaaro/pydub#installation"


from .beatmap import Beatmap
from .beatmapSet import BeatmapSet
from . import utils
from . import data_express

from . import beatmap, beatmapSet, utils, data_express
