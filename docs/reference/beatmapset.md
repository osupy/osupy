# BeatmapSet


???+ example

    ```py
    from osupy import BeatmapSet


    beatmapset_path = "path/of/osu/Songs/beatmapset"
    beatmap_set = BeatmapSet.from_file(beatmapset_path)
    print(beatmap_set)
    print(beatmap_set.title)
    print(beatmap_set.artist)
    for beatmap in beatmap_set.beatmaps:
        print(beatmap.pp, beatmap.stars)
    ```

## Reference

:::osupy.beatmapSet.BeatmapSet
    rendering:
      show_root_heading: no