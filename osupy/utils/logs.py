"""
Project: osupy

License MIT: https://mit-license.org/
Copyright © 2021 - LostPy
"""

import logging

try:
    from colorama import Style, Fore, init

    COLORS = {
        "success": Fore.GREEN,
        "critical": Fore.RED,
        "exception": Fore.RED,
        "error": Fore.RED,
        "warning": Fore.YELLOW,
        "info": Fore.WHITE,
        "debug": Fore.BLUE
    }

    colorama_imported = True
    init()

except ImportError:
    colorama_imported = False


class ColoredFormatter(logging.Formatter):

    def __init__(self, *args, use_color: bool = True, **kwargs):
        super().__init__(*args, **kwargs)
        self.use_color = use_color

    def format(self, record):
        msg = super().format(record)

        levelname = record.levelname
        if self.use_color and levelname.lower() in COLORS.keys():
            msg = COLORS[levelname.lower()] + msg + Style.RESET_ALL
        return msg


class OsuPyLogger(logging.Logger):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def success(self, msg: str, *args, **kwargs):
        self.log(45, msg, *args, **kwargs)


def set_level(logger, levelname: str):
    levels = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL,
        'success': 45
    }
    logger.setLevel(levels[levelname])


# --------------------
# Init of logger
# --------------------

logging.setLoggerClass(OsuPyLogger)
stream_formatter = ColoredFormatter(
    "[%(asctime)s][%(name)s][%(levelname)s] %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S"
)

osupy_logger = logging.getLogger('OsuPy')

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.DEBUG)
stream_handler.setFormatter(stream_formatter)
osupy_logger.addHandler(stream_handler)

