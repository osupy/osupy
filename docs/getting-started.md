# Getting Started

## Use the CLI

To use the CLI, use the command:

=== "Windows or in environment"
    ```bash
    python -m osupy
    ```

=== "Linux (default Python version)"

    ```bash
    python3 -m osupy
    ```

=== "Linux (specific Python version)"
    Replace `9` with the desired Python 3 version

    ```bash
    python3.9 -m osupy
    ```

With the help argument (`-h` or `--help`) or if you do not pass any arguments, you get the following help message:

```bash
usage: osupy [-h] [--limit LIMIT] [--beatmap] [--set] [--output {csv,xlsx}] [--play] [path]

Utility to export beatmaps from osu! folder into xlsx or csv file.

positional arguments:
  path                 The path of beatmap, beatmap set or osu! folder

optional arguments:
  -h, --help           show this help message and exit
  --limit LIMIT        The maximum of beatmap to export. Default: no limit
  --beatmap            If "path" is a beatmap. Display data of the beatmap.
  --set                If "path" is a beatmap set. Display data of the beatmap set.
  --output {csv,xlsx}  If path is the osu! folder, indicate the output format of data.
  --play               If path is a beatmap or beatmap set, play the music of beatmap set.
```

=== "Export in CSV"

    To export all beatmaps in a csv file, use the following command:

    ```bash
    python -m osupy path/of/osu --output csv
    ```

=== "Export in XLSX"

    To export all beatmaps in a xlsx file, use the following command:

    ```bash
    python -m osupy path/of/osu --output xlsx
    ```

=== "Get Beatmap data"

    To see data for a particular beatmap, use the following command:
    ```bash
    python -m osupy path/of/osu/Songs/beatmapset/beatmap.osu --beatmap
    ```

=== "Get Beatmap set data"

    To see data for a particular beatmap set, use the following command:
    ```bash
    python -m osupy path/of/osu/Songs/beatmapset --set
    ```

You can limit the number of beatmap sets exported by adding the argument `--limit`:

=== "Export in CSV"

    ```bash
    python -m osupy path/of/osu --output csv --limit 100
    ```

=== "Export in XLSX"

    ```bash
    python -m osupy path/of/osu --output xlsx --limit 100
    ```

## Use the package in a script


### Load a beatmap set

To load a beatmap set in a variable, you can use the class method `from_folder`of `BeatmapSet` class:

```{.py3 hl_lines="5"}
from osupy import BeatmapSet


set_path = "path/of/beatmapset"
my_set = BeatmapSet.from_folder(set_path)
```

You can see all beatmap of the set with the  `beatmaps` attribute:

```py
print(my_set.beatmaps)
```

### Performance calculation

To calcul pp of a beatmap, use the `calculate` method of a `Beatmap` instance. the possible arguments are those of the `__init__` function of the `Calculator` class of [peace_performance_python][ppp] package.

```{.py3 hl_lines="7"}
from osupy import BeatmapSet


set_path = "path/of/beatmapset"
my_set = BeatmapSet.from_folder(set_path)

result = my_set.beatmaps[0].calculate(acc=96.5, miss=2)
print(result)
```

!!! info

    All initialized beatmaps with class methods `osupy.Beatmap.from_file` or `osupy.BeatmapSet.from_folder`  already have a `pp` attribute giving the performance with an accuracy of 100 and without mods.


### Export beatmap set data to a dataframe

!!! warning

    The methods used in this section uses the pandas package:

    === "Windows or in environment"

        ```bash
        pip install pandas
        ```

    === "Linux"

        ```bash
        pip3 install pandas
        ```

To export beatmap set data to a `pandas.DataFrame`, you can use the `to_dataframe` methode.

```{.py3 hl_lines="6"}
from osupy import BeatmapSet


set_path = "path/of/beatmapset"
my_set = BeatmapSet.from_folder(set_path)
df = my_set.to_dataframe()
print(df)
```

You can also export the hitobjects data of a beatmap to a dataframe with the `hitobjects_data` method:

```{.py3 hl_lines="6"}
from osupy import BeatmapSet


set_path = "path/of/beatmapset"
my_set = BeatmapSet.from_folder(set_path)
df = my_set[0].hitobjects_data()  # export hitobjects data from the first beatmap of `my_set` to a dataframe
print(df)
```

[ppp]: https://github.com/Pure-Peace/peace-performance-python
