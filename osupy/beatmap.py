"""beatmap.py

--------------------
Project: osupy

License MIT: https://mit-license.org/
Copyright © 2021 - LostPy
--------------------
"""
import logging

try:
    import numpy as np
    import pandas as pd
    PANDAS_IMPORTED = True
except ModuleNotFoundError:
    logging.warning(
        "Package not found: 'numpy' and/or 'pandas'."
        "It will be impossible of export beatmap data to 'pandas.DataFrame'")
    PANDAS_IMPORTED = False
    from . import __PANDA_NOT_FOUND_MESSAGE

import peace_performance_python.prelude as osu_prelude


# Class:
class Beatmap(osu_prelude.Beatmap):
    """Class to represent a beatmap with this data.

    It's a subclass of [`peace_performance_python.prelude.Beatmap`](https://github.com/Pure-Peace/peace-performance-python/blob/main/peace_performance_python/objects/beatmap.py)
    """
    NO_MOD_MAX_CALCULATOR = osu_prelude.Calculator(acc=100)

    def __init__(self, path: str):
        """Use static method `from_beatmap` to create a Beatmap objects with a .osu file.

        Parameters
        ----------
        path : str
            The path of beatmap. Example: `"path/of/osu/Songs/beatmapset/beatmap.osu"`
        """
        super().__init__(path)
        self._default_calculate()

    def __repr__(self) -> str:
        """Returns some beatmap data (path, stars and pp) if pandas isn't imported
        else the pandas representation of beatmap

        Returns
        -------
        str
            The beatmap representation.
            If pandas is imported, return `pd.DataFrame.__repr__`,
            else, return `f"<[{self.path}] {self.stars}* ({self.pp})>"`
        """
        if PANDAS_IMPORTED:
            return self.to_dataframe().__repr__()
        return f"<[{self.path}] {self.stars}* ({self.pp})>"

    def __str__(self) -> str:
        """Returns the str of the dataframe of metadata.
        
        Returns
        -------
        str
            return `self.__repr__()`
        """
        return self.__repr__()

    def __len__(self) -> float:
        """Returns the time of the beatmap.

        Returns
        -------
        float
            return the time of beatmap (`self.time`)
        """
        return self.time

    def __eq__(self, obj: 'Beatmap') -> bool:
        """Compare the difficulty.

        Parameters
        ----------
        obj : Beatmap
            The beatmap to compare with self

        Returns
        -------
        bool
            Returns True if both beatmaps have the same difficulty (`stars` attribute)

        Raises
        ------
        TypeError
            If `obj` is not an instance of Beatmap.
        """
        if isinstance(obj, Beatmap):
            return self.stars == obj.stars

        raise TypeError("You can't compare an instance of Beatmap with another object")

    def __ne__(self, obj: 'Beatmap') -> bool:
        """Compare the difficulty.

        Parameters
        ----------
        obj : Beatmap
            The beatmap to compare with self

        Returns
        -------
        bool
            Returns True if both beatmaps have not the same difficulty (`stars` attribute)

        Raises
        ------
        TypeError
            If `obj` is not an instance of Beatmap.
        """
        if isinstance(obj, Beatmap):
            return self.stars != obj.stars

        raise TypeError("You can't compare an instance of Beatmap with another object")

    def __gt__(self, obj: 'Beatmap') -> bool:
        """Compare the difficulty.

        Parameters
        ----------
        obj : Beatmap
            The beatmap to compare with self

        Returns
        -------
        bool
            Returns True if the difficulty difficulty of `self` is greater than `obj` (`stars` attribute)

        Raises
        ------
        TypeError
            If `obj` is not an instance of Beatmap.
        """
        if isinstance(obj, Beatmap):
            return self.stars > obj.stars

        raise TypeError("You can't compare an instance of Beatmap with another object")

    def __ge__(self, obj: 'Beatmap') -> bool:
        """Compare the difficulty.

        Parameters
        ----------
        obj : Beatmap
            The beatmap to compare with self

        Returns
        -------
        bool
            Returns True if the difficulty difficulty of `self` is greater or equal than `obj` (`stars` attribute)

        Raises
        ------
        TypeError
            If `obj` is not an instance of Beatmap.
        """
        if isinstance(obj, Beatmap):
            return self.stars >= obj.stars

        raise TypeError("You can't compare an instance of Beatmap with another object")

    def __lt__(self, obj: 'Beatmap') -> bool:
        """Compare the difficulty.

        Parameters
        ----------
        obj : Beatmap
            The beatmap to compare with self

        Returns
        -------
        bool
            Returns True if the difficulty difficulty of `self` is lower than `obj` (`stars` attribute)

        Raises
        ------
        TypeError
            If `obj` is not an instance of Beatmap.
        """
        if isinstance(obj, Beatmap):
            return self.stars < obj.stars

        raise TypeError("You can't compare an instance of Beatmap with another object")

    def __le__(self, obj: 'Beatmap') -> bool:
        """Compare the difficulty.

        Parameters
        ----------
        obj : Beatmap
            The beatmap to compare with self

        Returns
        -------
        bool
            Returns True if the difficulty difficulty of `self` is lower or equal than `obj` (`stars` attribute)

        Raises
        ------
        TypeError
            If `obj` is not an instance of Beatmap.
        """
        if isinstance(obj, Beatmap):
            return self.stars <= obj.stars

        raise TypeError("You can't compare an instance of Beatmap with another object")

    def _default_calculate(self):
        """Calculating difficulty and performance with the default calculator.
        """
        result = Beatmap.NO_MOD_MAX_CALCULATOR.calculate(self)
        self._stars, self._pp = result.stars, result.pp

    @property
    def version_fmt(self) -> int:
        """Returns the version of .osu file format.

        Returns
        -------
        int
            The version format
        """
        return self.version

    @property
    def stars(self) -> float:
        """Returns the difficulty of beatmap (stars).

        Returns
        -------
        float
            The stars number
        """
        return self._stars

    @property
    def pp(self) -> float:
        """Returns the max pp number (no mod).

        Returns
        -------
        float
            The performance point in No Mod and 100% of acc
        """
        return self._pp

    @property
    def time(self) -> float:
        """Returns the difference between the first hit object and the last hit object.

        Returns
        -------
        float
            The duration of the beatmap. The time between the first hit object and last hit object.
        """
        hitobjects = self.hit_objects
        if len(hitobjects) > 1:
            return hitobjects[-1].end_time - hitobjects[0].start_time
        return 0

    @property
    def hitobjects_data(self) -> 'pd.DataFrame':
        """Returns hitobjects data in a `pandas.DataFrame`

        Returns
        -------
        pd.DataFrame
            Hitobjects data

        Raises
        ------
        ModuleNotFoundError
            If `pandas` is not installed
        """
        if PANDAS_IMPORTED:
            hit_objects = self.hit_objects
            hit_objects_data = [
                {
                    'Type': hit_obj.kind_str,
                    'StartTime': hit_obj.start_time,
                    'EndTime': hit_obj.end_time,
                    'Sound': hit_obj.sound,
                    'X': hit_obj.pos.x,
                    'Y': hit_obj.pos.y,
                    'PathType': np.nan if hit_obj.kind.path_type is None else hit_obj.kind.path_type,
                    'Kind': str(hit_obj.kind.as_dict)
                }
                for hit_obj in hit_objects
            ]
            return pd.DataFrame(hit_objects_data)
        raise ModuleNotFoundError(__PANDA_NOT_FOUND_MESSAGE)

    def calculate(self, *args, **kwargs) -> osu_prelude.CalcResult:
        """Create a `peace_performance_python.prelude.CalcResult` for this beatmap.

        Parameters
        ----------
        args
            Positional arguments of `peace_performance_python.prelude.Calculator.__init__`
        kwargs
            Keywords arguments of `peace_performance_python.prelude.Calculator.__init__`

        Returns
        -------
        peace_performance_python.prelude.CalcResult
            The result of the calculation. See [peace_performance_python doc](https://github.com/Pure-Peace/peace-performance-python/blob/main/peace_performance_python/_peace_performance/pp.pyi)
        """
        return osu_prelude.Calculator(*args, **kwargs).calculate(self)

    def to_dataframe(self) -> 'pd.DataFrame':
        """Returns a DataFrame with metadatas of a beatmap.

        Returns
        -------
        pd.DataFrame
            Beatmap data in a `pandas.DataFrame` (1 line)

        Raises
        ------
        ModuleNotFoundError
            If `pandas` is not installed
        """
        if PANDAS_IMPORTED:
            data = {
                'Version_fmt': self.version_fmt,
                'Mode': self.mode,
                'Stars': self.stars,
                'PP': self.pp,
                'HP': self.hp,
                'CS': self.cs,
                'OD': self.od,
                'AR': self.ar,
                'SV': self.sv,
                'TickRate': self.tick_rate,
                'CountNormal': self.n_circles,
                'CountSlider': self.n_sliders,
                'CountSpinner': self.n_spinners,
                'Time': self.time
            }
            return pd.DataFrame(data=data, index=range(1), columns=data.keys())
        raise ModuleNotFoundError(__PANDA_NOT_FOUND_MESSAGE)

    @classmethod
    def from_file(cls, filepath: str):
        """Returns a Beatmap instance with all data find in filepath.

        Parameters
        ----------
        filepath : str
            The path of beatmap. Example: `"path/of/osu/Songs/beatmapset/beatmap.osu"`

        Returns
        -------
        Beatmap
            The new Beatmap instance
        """
        beatmap = cls(filepath)
        return beatmap

    @classmethod
    def new(cls, *args, **kwargs):
        """Create a new Beatmap"""
        pass
