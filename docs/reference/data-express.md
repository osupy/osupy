# `data_express` module


## Export data

???+ example

    ```py
    from osupy.data_express import osu_to_csv, osu_to_excel, from_osu


    OSU_PATH = "path/of/osu"

    # Export all beatmaps data to a csv file
    osu_to_csv(OSU_PATH, 'osu_data.csv', compact_log=True, display_progress=True)

    # Export all beatmaps data to a xlsx file
    osu_to_excel(OSU_PATH, 'osu_data.xlsx', compact_log=True, display_progress=True)

    # Export all beatmaps data to a `pandas.DataFrame`
    osu_df = from_osu(OSU_PATH, compact_log=True, display_progress=True)
    ```

:::osupy.data_express.export

## Display data

???+ example

    ```py
    from osupy.data_express import from_osu, version_fmt

    OSU_PATH = "path/of/osu"

    # Export all beatmaps data to a `pandas.DataFrame`
    osu_df = from_osu(OSU_PATH, compact_log=True, display_progress=True)

    # display version format stats in a matplotlib graph
    version_fmt(osu_df)
    ```

!!! warning

    Only `version_fmt`, `data_add` and `play_music` functions are implemented.