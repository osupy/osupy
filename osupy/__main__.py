"""
Project: osupy

License MIT: https://mit-license.org/
Copyright © 2021 - LostPy
"""

import sys
import os.path
import argparse

try:
    from osupy.data_express import (
        osu_to_csv, osu_to_excel,
        from_beatmap, from_folder
    )
    from osupy import play_music
    from utils import osupy_logger
except ImportError as e:
    import logging
    logging.debug(e)
    raise ModuleNotFoundError(
        "This CLI use pandas package."
        " Please, install pandas: `pip install pandas`")


DESCRIPTION = "Utility to export beatmaps from osu!"\
    " folder into xlsx or csv file."


def create_parser():
    parser = argparse.ArgumentParser("osupy", description=DESCRIPTION)
    parser.add_argument(
        'path', type=str, nargs='?',
        help='The path of beatmap, beatmap set or osu! folder')
    parser.add_argument(
        '--limit', type=int, default=None,
        help='The maximum of beatmap to export. Default: no limit')
    parser.add_argument(
        '--beatmap', action='store_true',
        help='If "path" is a beatmap. Display data of the beatmap.')
    parser.add_argument(
        '--set', action='store_true',
        help='If "path" is a beatmap set. Display data of the beatmap set.')
    parser.add_argument(
        '--output', choices=['csv', 'xlsx'], default='csv',
        help='If path is the osu! folder, indicate the output format of data.')
    parser.add_argument(
        '--play', action='store_true',
        help='If path is a beatmap or beatmap set,'
        ' play the music of beatmap set.')

    return parser


def main():
    parser = create_parser()
    args = parser.parse_args(sys.argv[1:])
    if args.path:
        msg_folder_expected = "The path must be a folder!"\
            " If you want get data from a beatmap, add '--beatmap' in command"
        if args.beatmap:
            if os.path.isdir(args.path) or\
                    os.path.splitext(args.path)[1] != '.osu':
                print("The path must be a .osu file!")
                parser.print_help()
                return
            print(from_beatmap(args.path))

        elif args.set:
            if os.path.isfile(args.path):
                print(msg_folder_expected)
                parser.print_help()
                return
            metadata, hitobjects = from_folder(args.path)
            print("metadata:\n", metadata, end='\n\n')
            print("hitobjects:\n", hitobjects)

        elif args.output == 'csv':
            if os.path.isfile(args.path):
                print(msg_folder_expected)
                parser.print_help()
                return
            csv_path = osu_to_csv(args.path, n=args.limit)
            osupy_logger.info(f"csv file save in {os.path.abspath(csv_path)}")

        elif args.output == 'xlsx':
            if os.path.isfile(args.path):
                print(msg_folder_expected)
                parser.print_help()
                return
            excel_path = osu_to_excel(args.path, n=args.limit)
            osupy_logger.info(
                f"Excel file save in {os.path.abspath(excel_path)}")

        if args.play and (args.beatmap or args.set):
            play_music(args.path if args.set else os.path.dirname(args.path))
    else:
        print("'path' must be specified\n\n")
        parser.print_help()


if __name__ == '__main__':
    main()
