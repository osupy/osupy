# Beatmap

???+ example

  ```py
  from osupy import Beatmap


  beatmap_path = "path/of/osu/Songs/beatmapset/beatmap.osu"
  beatmap = Beatmap.from_file(beatmap_path)
  print(beatmap)
  print(beatmap.pp)
  print(beatmap.stars)
  ```

## Reference

:::osupy.beatmap.Beatmap
    rendering:
      show_root_heading: no