# OsuPy

A Python package for [osu][osu].  
This package is used to get beatmap data and to edit beatmap.
The documentation can be found [here][doc].

This package replace [OsuData][osudata] package.

## Installation

To install this package, you can use:

```
pip install git+https://gitlab.com/osupy/osupy.git
```

To use an other branch:

```
pip install git+https://gitlab.com/osupy/osupy.git@<branch name>
```

## Requirements

 * [peace-performance-python][ppp]: The package use to read and calculate stats of beatmap

### Optional

 * [pandas][pd]: To export data in DataFrame, Excel or CSV
 * [pydub][pydub]: To use mp3
 * [scipy][scipy]: To export audio data

## License

This package is under [MIT license][mit]:

> Copyright © 2021 - LostPy  
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:  
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.  
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


[doc]: https://osupy.gitlab.io/osupy
[osu]: https://osu.ppy.sh/home
[osudata]: https://gitlab.com/osupy/OsuData
[ppp]: https://github.com/Pure-Peace/peace-performance-python
[pd]: https://pandas.pydata.org/
[pydub]: https://github.com/jiaaro/pydub
[scipy]: https://docs.scipy.org/doc/scipy/index.html
[mit]: https://mit-license.org/