"""
License MIT: https://mit-license.org/
Copyright © 2021 - LostPy

A module to export data from osu!
"""

import os
import time
from typing import List, Tuple

import pandas as pd

try:
    import pydub
    PYDUB_IMPORTED = True
except ModuleNotFoundError:
    PYDUB_IMPORTED = False

from osupy.utils import osupy_logger, progress_bar
from osupy import Beatmap, BeatmapSet
from osupy.beatmapError import BeatmapError


MAX_EXCEL_LINES = 1048576  # limit of lines in a xlsx file


def to_csv(folderpath: str, csv_path: str = '') -> str:
    """Export beatmap set data to a csv file

    Parameters
    ----------
    folderpath : str
        beatmap set path. Example: `"path/of/osu/Songs/beatmapset"`
    csv_path : str, optional
        The csv path

    Returns
    -------
    str
        The csv path
    """
    beatmap_set = BeatmapSet.from_folder(folderpath)
    csv_path = f'./{beatmap_set.title}.csv' if not csv_path else csv_path
    beatmap_set.to_csv(csv_path)
    return csv_path


def to_excel(folderpath: str, excel_path: str = '', **kwargs) -> str:
    """Export beatmap set data to a xlsx file

    Parameters
    ----------
    folderpath : str
        beatmap set path. Example: `"path/of/osu/Songs/beatmapset"`
    excel_path : str, optional
        The xlsx path
    **kwargs
        Other arguments to pass to the `pandas.DataFrame.to_excel` method

    Returns
    -------
    str
        The xlsx path
    """
    beatmap_set = BeatmapSet.from_folder(folderpath)
    metadatas = beatmap_set.to_dataframe()
    hitobjects = beatmap_set.dataframe_hitobjects()
    excel_path = f'./{beatmap_set.title}.xlsx' if not excel_path else excel_path
    with pd.ExcelWriter(excel_path, mode='w') as writer:
        for df, name in zip([metadatas, hitobjects], ['metadatas', 'hitobjects']):
            df.to_excel(writer, sheet_name=name, **kwargs)
    return excel_path


def mp3_to_wav(mp3_path: str, wav_path: str = '') -> str:
    """Convert mp3 to wav

    Parameters
    ----------
    mp3_path : str
        The mp3 path
    wav_path : str, optional
        The output path

    Returns
    -------
    str
        The wav path
    """
    if PYDUB_IMPORTED:
        if wav_path == '':
            wav_path = './mp3_to_wav.wav'
        mp3 = pydub.AudioSegment.from_mp3(mp3_path)
        mp3.export(wav_path, format="wav")
        return wav_path
    raise ModuleNotFoundError(
        "No module named 'pydub'. Please, install pydub to use this function")


def beatmapSet_objects(
        osu_path: str,
        n: int = None,
        compact_log: bool = False,
        display_progress: bool = True
    ) -> List[BeatmapSet]:
    """
    A function to extract osu! beatmaps data and return the list of BeatmapSet object
    and the list path of beatmaps where there is a error.

    Parameters
    ----------
    osu_path : str
        The osu path. Example: `"path/of/osu"`
    n : int, optional
        The max number of beatmap set to load
    compact_log : bool, optional
        Compact log. Default: False
    display_progress : bool, optional
        Displa the progression if True.

    Returns
    -------
    List[BeatmapSet]
        The list of BeatmapSet with all beatmap set loaded.
    """

    beatmap_set_objects = []
    if display_progress:
        osupy_logger.info(f"Playback of all .osu files in '{osu_path}' will start.")
        osupy_logger.info("This action may take some time depending on the amount of files to be read...\n\n")
        time.sleep(5)
    songspath = os.path.join(osu_path, 'Songs/')
    list_dir = os.listdir(songspath)

    if n is None or n > len(list_dir):
        n = len(list_dir)
    n_init = n
    speed = 0.
    current_speed = 0.

    for i, name in enumerate(list_dir):
        if os.path.isdir(os.path.join(songspath, name)):
            start = time.time()

            if display_progress:  # update of the progress bar
                i_real = i - (n - n_init) if n < len(list_dir) else i
                progress_bar(
                    i_real,
                    n_init,
                    start=0 if i == i_real else -1,
                    info=os.path.join(songspath, name),
                    length=60,
                    suffix=f'Directories - ({current_speed} dir/s - mean: {speed} dir/s)',
                    compact=compact_log
                )

            beatmap_set = BeatmapSet.from_folder(os.path.join(songspath, name))
            beatmap_set_objects.append(beatmap_set)

            end = time.time()
            delay = end - start
            if delay != 0:
                speed = round((1 / delay + speed * i) / (i + 1), ndigits=3)
                current_speed = round(1 / delay, ndigits=3)

            if i == n_init:
                break
    return beatmap_set_objects


def from_beatmap(filepath: str) -> pd.DataFrame:
    """Function to extract metadatas of a beatmap.

    Parameters
    ----------
    filepath : str
        The beatmap path. Example: `"path/of/osu/Songs/beatmapset/beatmap.osu"`.

    Returns
    -------
    pd.DataFrame
        beatmap data
    """
    beatmap = Beatmap.from_file(filepath)
    return beatmap.to_dataframe()


def from_folder(folderpath: str) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """A function read and extract beatmaps datas of a folder.

    Parameters
    ----------
    folderpath : str
        The beatmap set folder. Example: `"path/of/osu/Songs/beatmapset"`

    Returns
    -------
    Tuple[pd.DataFrame, pd.DataFrame]
        beatmap set data and hitobjects data
    """
    beatmap_set = BeatmapSet.from_folder(folderpath)
    metadata = beatmap_set.to_dataframe()
    hitobjects_data = beatmap_set.dataframe_hitobjects()
    return metadata, hitobjects_data


def from_osu(
        osu_path: str,
        n: int = None,
        compact_log: bool = False,
        display_progress: bool = True
) -> pd.DataFrame:
    """A function to extract beatmaps data from osu! folder and
    return two dataframe and a list of path where there is a error.

    Parameters
    ----------
    osu_path : str
        The osu! path. Example: `"path/os/osu"`
    n : int, optional
        The max number of beatmap set to load.
    compact_log : bool, optional
        Compact log. Default: False
    display_progress : bool, optional
        Displa the progression if True.

    Returns
    -------
    pd.DataFrame
        All beatmaps data from osu! folder
    """
    beatmap_set_objects = beatmapSet_objects(
        osu_path,
        n=n,
        compact_log=compact_log,
        display_progress=display_progress
    )

    osupy_logger.info("Merge all data...")
    metadata = pd.concat(
        [beatmap_set.to_dataframe() for beatmap_set in beatmap_set_objects],
        axis=0
    ).reset_index(drop=True)

    osupy_logger.success("Merge all data completed")
    return metadata


def osu_to_csv(
        osu_path: str,
        csv_path: str = '',
        n: int = None,
        compact_log: bool = False,
        display_progress=True
) -> str:
    """Export metadata of all beatmaps from osu! folder in a csv file.

    Parameters
    ----------
    osu_path : str
        The osu! path. Example: `"path/of/osu"`
    csv_path : str, optional
        The csv path
    n : int, optional
        The max number of beatmap set to load.
    compact_log : bool, optional
        Compact log. Default: False
    display_progress : bool, optional
        Displa the progression if True.

    Returns
    -------
    str
        The csv path
    """
    metadata = from_osu(
        osu_path,
        n=n,
        compact_log=compact_log,
        display_progress=display_progress
    )
    osupy_logger.info("the csv file is being created...")
    csv_path = './osu_data.csv' if not csv_path else csv_path
    metadata.to_csv(csv_path, sep='$', index=False)
    return csv_path


def osu_to_excel(
        osu_path: str,
        excel_path: str = '',
        n: int = None,
        compact_log: bool = False,
        display_progress=True,
        **kwargs
) -> str:
    """Export metadata of all beatmaps from osu! folder in a xlsx file.

    Parameters
    ----------
    osu_path : str
        The osu! path. Example: `"path/of/osu"`
    excel_path : str, optional
        The xlsx path
    n : int, optional
        The max number of beatmap set to load.
    compact_log : bool, optional
        Compact log. Default: False
    display_progress : bool, optional
        Displa the progression if True.
    **kwargs
        Other arguments to pass to the `pandas.DataFrame.to_excel` method

    Returns
    -------
    str
        The xlsx path
    """
    metadata = from_osu(
        osu_path,
        n=n,
        compact_log=compact_log,
        display_progress=display_progress
    )
    mode = 'w' if not excel_path.strip() else 'a'
    excel_path = './osu_data.xlsx' if not excel_path else excel_path
    with pd.ExcelWriter(excel_path, mode=mode) as writer:
        osupy_logger.info("the 'metadata' sheet is being created...")
        metadata[:MAX_EXCEL_LINES].to_excel(writer, sheet_name='metadata', index=False, **kwargs)

    if metadata.shape[0] > MAX_EXCEL_LINES:
        osupy_logger.warning(f'The sheet "metadata" is too large ({metadata.shape[0]} lines), the maximum size has been keeping (MAX_EXCEL_LINES)')
    else:
        osupy_logger.success('There is not error during the export data')
    return excel_path
