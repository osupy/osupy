"""
Project: OsuData

License MIT: https://mit-license.org/
Copyright © 2020-2021 - LostPy
"""

from .logs import OsuPyLogger, osupy_logger, set_level as _set_level
from .osuProgressBar import progress_bar


def set_logger_level(levelname: str):
    _set_level(osupy_logger, levelname)
