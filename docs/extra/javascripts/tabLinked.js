

const tabs = document.querySelectorAll(".tabbed-set > input");

function labelWithSameContent(label1, label2) {
	return label1.innerHTML === label2.innerHTML;
}


function updateSelectedTabs(event) {
	const tab = event.target;
	const current = document.querySelector(`label[for=${tab.id}]`);
	const pos = current.getBoundingClientRect().top;  // To preserve scroll position
	const labels = document.querySelectorAll('.tabbed-set > label, .tabbed-alternate > .tabbed-labels > label');

	for (const label of labels) {
		if (labelWithSameContent(current, label)) {
			document.querySelector(`input[id=${label.getAttribute('for')}]`).checked = true;
		}
	}

	// Preserve scroll position
    const delta = (current.getBoundingClientRect().top) - pos;
    window.scrollBy(0, delta);
}


for (const tab of tabs) {
	tab.addEventListener("click", updateSelectedTabs);
}