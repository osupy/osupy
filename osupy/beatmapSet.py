"""
Project: osupy

License MIT: https://mit-license.org/
Copyright © 2021 - LostPy
"""

import os
import logging
from datetime import datetime

try:
    import pandas as pd
    PANDAS_IMPORTED = True
except ModuleNotFoundError:
    PANDAS_IMPORTED = False
    from . import __PANDA_NOT_FOUND_MESSAGE

try:
    import pydub
    from pydub.playback import play
    from scipy.io import wavfile
    PYDUB_IMPORTED = True
except ModuleNotFoundError:
    PYDUB_IMPORTED = False
    logging.warning("Package not found: 'scipy' and or 'pydub'. It will be impossible to export mp3 file in wavfile and to get data from mp3.")
    from . import __PYDUB_NOT_FOUND_MESSAGE

from .beatmap import Beatmap


class BeatmapSet:
    """Class to represent a beatmap set with this data.

    Attributes
    ----------

    folderpath : str
        The path of beatmap set. Example: `"path/of/osu/Songs/beatmapset"`
    id : int, optional
        The beatmap set id. Can be `None`.
    music_path : str, optional
        The music path for this beatmap set
    title : str, optional
        The beatmap set title
    artist : str
        The music artist
    beatmaps : list[Beatmap]
        The list of beatmap (`osupy.Beatmap`)
    date_add : datetime.datetime, optional
        The date the beatmap set was added
    """

    def __init__(self, folderpath: str, id_: int = None):
        self.folderpath = folderpath
        self.id = id_
        self.music_path = None
        self.title = None
        self.artist = None
        self.beatmaps = []
        self.date_add = None

    def __repr__(self) -> str:
        """Returns some beatmap data (path, title, artist) if pandas isn't imported
        else the pandas representation of beatmap
        """
        if PANDAS_IMPORTED:
            return self.to_dataframe().__repr__()
        return f"[{self.folderpath}] {self.title} ({self.artist})"

    def __str__(self) -> str:
        """Returns the str of the dataframe of metadata."""
        return self.__repr__()

    def __len__(self) -> int:
        """Returns number of beatmaps."""
        return len(self.beatmaps)

    def __eq__(self, obj) -> bool:
        """Compare with the number of beatmaps.

        Parameters
        ----------
        obj : BeatmapSet
            The other beatmap set to compare

        Returns
        -------
        bool
            Returns True if both beatmaps have the same number of beatmaps

        Raises
        ------
        TypeError
            if `obj` is not an instance of BeatmapSet.
        """
        if isinstance(obj, BeatmapSet):
            return len(self.beatmaps) == len(obj.beatmaps)

        raise TypeError("You can't compare an instance of BeatmapSet with another object")

    def __ne__(self, obj) -> bool:
        """Compare with the number of beatmaps.

        Parameters
        ----------
        obj : BeatmapSet
            The other beatmap set to compare

        Returns
        -------
        bool
            Returns True if both beatmaps have not the same number of beatmaps

        Raises
        ------
        TypeError
            if `obj` is not an instance of BeatmapSet.
        """
        if isinstance(obj, BeatmapSet):
            return len(self.beatmaps) != len(obj.beatmaps)

        raise TypeError("You can't compare an instance of BeatmapSet with another object")

    def __gt__(self, obj) -> bool:
        """Compare with the number of beatmaps.

        Parameters
        ----------
        obj : BeatmapSet
            The other beatmap set to compare

        Returns
        -------
        bool
            Returns True if the number of beatmaps of `self` is greater than `obj`

        Raises
        ------
        TypeError
            if `obj` is not an instance of BeatmapSet.
        """
        if isinstance(obj, BeatmapSet):
            return len(self.beatmaps) > len(obj.beatmaps)

        raise TypeError("You can't compare an instance of BeatmapSet with another object")

    def __ge__(self, obj) -> bool:
        """Compare with the number of beatmaps.

        Parameters
        ----------
        obj : BeatmapSet
            The other beatmap set to compare

        Returns
        -------
        bool
            Returns True if the number of beatmaps of `self` is greater or equal than `obj`

        Raises
        ------
        TypeError
            if `obj` is not an instance of BeatmapSet.
        """
        if isinstance(obj, BeatmapSet):
            return len(self.beatmaps) >= len(obj.beatmaps)

        raise TypeError("You can't compare an instance of BeatmapSet with another object")

    def __lt__(self, obj) -> bool:
        """Compare with the number of beatmaps.

        Parameters
        ----------
        obj : BeatmapSet
            The other beatmap set to compare

        Returns
        -------
        bool
            Returns True if the number of beatmaps of `self` is lower than `obj`

        Raises
        ------
        TypeError
            if `obj` is not an instance of BeatmapSet.
        """
        if isinstance(obj, BeatmapSet):
            return len(self.beatmaps) < len(obj.beatmaps)

        raise TypeError("You can't compare an instance of BeatmapSet with another object")

    def __le__(self, obj) -> bool:
        """Compare with the number of beatmaps.

        Parameters
        ----------
        obj : BeatmapSet
            The other beatmap set to compare

        Returns
        -------
        bool
            Returns True if the number of beatmaps of `self` is lower or equal than `obj`

        Raises
        ------
        TypeError
            if `obj` is not an instance of BeatmapSet.
        """
        if isinstance(obj, BeatmapSet):
            return len(self.beatmaps) <= len(obj.beatmaps)

        raise TypeError("You can't compare an instance of BeatmapSet with another object")

    def __getitem__(self, index: int) -> Beatmap:
        """Get beatmap with a index.

        Parameters
        ----------
        index : int
            Index of beatmap in the list

        Returns
        -------
        Beatmap
        """
        return self.beatmaps[index]

    def __setitem__(self, index: int, beatmap: 'Beatmap'):
        """Insert a beatmap in the list of beatmaps.

        Parameters
        ----------
        index : int
            index of beatmap
        beatmap : Beatmap
            The beatmap to add

        Raises
        ------
        TypeError
            if beatmap argument is not an instance of Beatmap
        """
        if isinstance(beatmap, Beatmap):
            self.beatmaps.insert(index, beatmap)

        raise TypeError("The value must be an instance of Beatmap")

    def __delitem__(self, index: int):
        """Delete the beatmap with the index.

        Parameters
        ----------
        index : int
            Index of beatmap to delete
        """
        del(self.beatmaps[index])

    def __contains__(self, obj) -> bool:
        """Returns True if obj is in the list of beatmaps.

        Parameters
        ----------
        obj : object
            The object to test

        Returns
        -------
        bool
            True if obj is in the list of beatmaps
        """
        return obj in self.beatmaps

    def append(self, beatmap: 'Beatmap'):
        """Append beatmap in the list of beatmaps.

        Parameters
        ----------
        beatmap : Beatmap
            The beatmap to add

        Raises
        ------
        TypeError
            If beatmap argument is not an instance of Beatmap
        """
        if isinstance(beatmap, Beatmap):
            self.beatmaps.append(beatmap)

        raise TypeError("The value must be an instance of Beatmap")

    def pop(self, index: int = -1) -> Beatmap:
        """Use pop method on the list of beatmaps.

        Parameters
        ----------
        index : int, optional
            The index of beatmap. Default: The last beatmap

        Returns
        -------
        Beatmap
            The beatmap removed from list of beatmap
        """
        return self.beatmaps.pop(index=index)

    def metadata(self) -> dict:
        """Export BeatmapSet object in a dictionary.
        
        Returns
        -------
        dict
            Metadata of beatmap set, it's the `__dict__` attribute
        """
        return self.__dict__

    def load(self, mode: int = None):
        """
        Initialize BeatmapSet object from files of beatmaps.
        Use `modes` argument if you want get specifics modes.

        Parameters
        ----------
        mode : int, optional
            The mode of beatmap to load. Default: load all beatmaps
        """
        self.date_add = datetime.fromtimestamp(os.path.getctime(self.folderpath)).strftime('%Y-%m-%d %H:%M:%S')
        file_name = os.path.basename(self.folderpath)
        try:
            self.id = int(file_name.split(' ')[0])
        except ValueError as e:  # Beatmaps not published
            return
        folderpath_data = file_name.split(str(self.id))[1].split(' - ', 1)

        if len(folderpath_data) > 1:
            self.title = folderpath_data[1].strip()
        else:
            self.title = ''
        self.artist = folderpath_data[0].strip()

        for name in os.listdir(self.folderpath):
            path = os.path.join(self.folderpath, name)
            if os.path.isfile(path) and name.endswith((".osu", )):
                if self.music_path is None:  # Initialize BeatmapSet music path
                    with open(path, mode='r', encoding='utf8') as f:
                        lines = f.readlines()
                    self.music_path = os.path.join(self.folderpath, lines[3][lines[3].find(" ")+1:].strip())

                beatmap = Beatmap(path)
                if mode is None or beatmap.mode == mode:
                    self.beatmaps.append(beatmap)

    def to_dataframe(self) -> 'pd.DataFrame':
        """Export BeatmapSet object in a DataFrame.

        Returns
        -------
        pd.DataFrame
            beatmaps data with beatmap set data.

        Raises
        ------
        ModuleNotFoundError
            If `pandas` is not installed
        """
        if PANDAS_IMPORTED:
            if len(self.beatmaps) > 0:
                df = pd.concat([beatmap.to_dataframe() for beatmap in self.beatmaps], axis=0).reset_index(drop=True)
                df['Title'] = self.title
                df['Artist'] = self.artist
                df['Date_Add'] = self.date_add
            else:
                df = pd.DataFrame(columns=['Version_fmt', 'Mode', 'Title',
                    'Stars', 'PP', 'HP', 'CS', 'OD', 'AR', 'SV',
                    'TickRate', 'CountNormal', 'CountSlider', 'CountSpinner', 'Time', 'Date_Add'])
            return df
        raise ModuleNotFoundError(__PANDA_NOT_FOUND_MESSAGE)

    def dataframe_hitobjects(self) -> 'pd.DataFrame':
        """Returns a DataFrame with hitobjects of all beatmaps from BeatmapSet object.

        Returns
        -------
        pd.DataFrame
            hitobjects data of all beatmaps

        Raises
        ------
        ModuleNotFoundError
            If `pandas` is not installed
        """
        if PANDAS_IMPORTED:
            if len(self.beatmaps) > 0:
                return pd.concat([beatmap.hitobjects_data for beatmap in self.beatmaps], axis=0).reset_index(drop=True)
            return pd.DataFrame(columns=['Type', 'StartTime', 'EndTime', 'Sound', 'X', 'Y', 'PathType', 'Kind'])
        raise ModuleNotFoundError(__PANDA_NOT_FOUND_MESSAGE)

    def to_csv(self, path: str = None):
        """Export BeatmapSet object in a csv file.

        Parameters
        ----------
        path : str, optional
            The path of output file

        Returns
        -------
        None or str
            If path is None, returns the resulting csv format as a string. Otherwise returns None.

        Raises
        ------
        ModuleNotFoundError
            If `pandas` is not installed
        """
        if not PANDAS_IMPORTED:
            raise ModuleNotFoundError(__PANDA_NOT_FOUND_MESSAGE)
        return self.to_dataframe().to_csv(path, sep='$', index=False)

    def to_excel(self, path: str = None, sheet_name='', **kwargs):
        """Export BeatmapSet object in a xlsx file.

        Parameters
        ----------
        path : str, optional
            The path of output file
        sheet_name : str, optional
            The sheet name of Excel file
        kwargs
            Other arguments to pass to the `pandas.DataFrame.to_excel` method

        Returns
        -------
        None or str
            If path is None, returns the resulting csv format as a string. Otherwise returns None.

        Raises
        ------
        ModuleNotFoundError
            If `pandas` is not installed
        """
        if not PANDAS_IMPORTED:
            raise ModuleNotFoundError(__PANDA_NOT_FOUND_MESSAGE)
        sheet_name = self.title if sheet_name == '' else sheet_name
        return self.to_dataframe().to_excel(path, sheet_name=sheet_name, index=False, **kwargs)

    def mp3_object(self) -> 'pydub.AudioSegment':
        """Returns a mp3 object (module pydub).

        Returns
        -------
        pydub.AudioSegment
            The AudioSegment instance with mp3 data. See [pydub package](https://github.com/jiaaro/pydub)

        Raises
        ------
        ModuleNotFoundError
            If `pydub` or `scipy` are not installed
        """
        if not PYDUB_IMPORTED:
            raise ModuleNotFoundError(__PYDUB_NOT_FOUND_MESSAGE)
        return pydub.AudioSegment.from_mp3(self.music_path)

    def to_wav(self, name='audio_wav') -> str:
        """Export the mp3 file in a wav file.

        Parameters
        ----------
        name : str, optional
            The filename of wav file. Default: `"audio_wav"`.
            A `.wav` extension is added.

        Returns
        -------
        str
            The path of wav file

        Raises
        ------
        ModuleNotFoundError
            If `pydub` or `scipy` are not installed
        """
        if not PYDUB_IMPORTED:
            raise ModuleNotFoundError(__PYDUB_NOT_FOUND_MESSAGE)
        path = os.path.join(self.folderpath, name+'.wav')
        self.mp3_object().export(path, format="wav")
        return path

    def data_music(self):
        """Extract audio data of the mp3 file.

        Returns
        -------
        float
            The rate of music
        dict
            Audio data of music

        Raises
        ------
        ModuleNotFoundError
            If `pydub` or `scipy` are not installed
        """
        if not PYDUB_IMPORTED:
            raise ModuleNotFoundError(__PYDUB_NOT_FOUND_MESSAGE)
        path = self.to_wav()
        rate, audData = wavfile.read(path)
        os.remove(path)
        return rate, audData

    def music_to_dataframe(self) -> 'pd.DataFrame':
        """Returns a DataFrame with audio data of the music.

        Returns
        -------
        pd.DataFrame
            Audio data of music

        Raises
        ------
        ModuleNotFoundError
            If `pandas` `pydub` or `scipy` are not installed
        """
        if not PANDAS_IMPORTED:
            raise ModuleNotFoundError(__PANDA_NOT_FOUND_MESSAGE)
        rate, audData = self.data_music()
        return pd.DataFrame(data=audData, columns=['L', 'R'])

    def play_music(self):
        """Play the music. Note: This isn't a async method.

        Raises
        ------
        ModuleNotFoundError
             If `pydub` or `scipy` are not installed
        """
        if not PYDUB_IMPORTED:
            raise ModuleNotFoundError(__PYDUB_NOT_FOUND_MESSAGE)
        play(self.mp3_object())

    @classmethod
    def from_folder(cls, folderpath: str, mode: int = None):
        """
        Returns a BeatmapSet instance with all data find in folderpath.
        If `mode` is None, all beatmap are imported.

        Parameters
        ----------
        folderpath : str
            The path of beatmap set. Example: `"path/of/osu/Songs/beatmapset"`
        mode : int, optional
            Description

        Returns
        -------
        BeatmapSet
            The new instance of BeatmapSet
        """
        beatmap_set = cls(folderpath)
        beatmap_set.load(mode=mode)
        return beatmap_set
