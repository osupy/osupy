"""
Project: osupy

License MIT: https://mit-license.org/
Copyright © 2021 - LostPy
"""

try:
    import pandas  # just to check if pandas exist in the env
    from .export import (
        to_csv,
        to_excel,
        osu_to_csv,
        osu_to_excel,
        mp3_to_wav,
        beatmapSet_objects,
        from_beatmap,
        from_folder,
        from_osu
    )

    from .info import (
        global_info,
        difficulties,
        date_add,
        time,
        version_fmt,
        beatmap_error,
        beatmap_data,
        folder_data,
        play_music
    )
    del pandas
except ImportError:
    pass
